<!-- Emacs: -*- coding: utf-8; eval: (auto-fill-mode -1); eval: (visual-line-mode t) -*- -->

# IFT-[47]902 Classes de maitre

Ce projet contient les diapositives des classes de maitre du cours [IFT-4902 Programmation avec R pour l'analyse de données](https://www.ulaval.ca/etudes/cours/ift-4902-programmation-avec-r-pour-lanalyse-de-donnees) offert par l'[École d'actuariat](https://www.act.ulaval.ca) de l'[Université Laval](https://ulaval.ca).

## Auteur

Vincent Goulet, professeur titulaire, École d'actuariat, Université Laval

## Licence

«IFT-[47]902 Classes de maitre» est mis à disposition sous licence [Attribution-Partage dans les mêmes conditions 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) de Creative Commons.

Consulter le fichier `LICENSE` pour la licence complète.

## Obtenir les diapositives

Pour obtenir les diapositives en format PDF, consulter la [page des versions](https://gitlab.com/vigou3/ift-4902-classes-maitre/-/releases) (*releases*).
