%%% Copyright (C) 2023-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet «IFT-[47]902 Classes de maitre»
%%% https://gitlab.com/vigou3/ift-4902-classes-maitre
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\section{Classe de maitre 1}

\begin{frame}[plain]
  \questionbox{Pouvez-vous revenir sur le concept de l'itération et
    récursion? L'itération s'illustre plus facilement, mais j'éprouve
    plus de difficulté à comprendre la récursion avec les exemples qui
    ont été donnés dans le chapitre 2.}
\end{frame}

\begin{frame}[plain]
  \emph{Pour comprendre la récursion il faut d'abord comprendre la
    récursion}
\end{frame}

\begin{frame}
  \frametitle{Tri par insertion}
  \pause

  \textbf{Itératif} \\
  «Pour toutes les cartes à partir de la deuxième, choisir la
  carte et la placer au bon endroit parmi les cartes déjà triées»
  \bigskip\pause

  \textbf{Récursif} \\
  «Placer la deuxième carte au bon endroit parmi les cartes déjà
  triées, puis trier par insertion les autres cartes (jusqu'à ce qu'il
  n'y en ait plus)»
\end{frame}

\begin{frame}
  \frametitle{Élévation à une puissance}
  \pause

  \textbf{Itératif} \\
  «Multiplier le nombre $b$ par lui-même $n$ fois»
  \bigskip\pause

  \textbf{Récursif} \\
  «Multiplier le nombre $b$ par $b$ élevé à la puissance $n - 1$ (avec
  $b^0 = 1$)»
\end{frame}

\begin{frame}[fragile]
  \frametitle{Algorithme 2.7}

  Basé sur
  \begin{equation*}
    b^n = b \times b^{n - 1}, \quad b^0 = 1.
  \end{equation*}

  Élever un nombre réel $b$ à la puissance entière positive $n$.
\begin{Verbatim}[commandchars=\\\{\}]
Puissance(nombre réel b, entier positif n)
  Si (n = 0)
    Retourner 1
  Retourner b \times Puissance(b, n - 1)
Fin Puissance
\end{Verbatim}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Version itérative (langage naturel)}

  Basé plutôt sur
  \begin{equation*}
    b^n = b \times b \times \cdots \times b.
  \end{equation*}

  Élever un nombre réel $b$ à la puissance entière positive $n$.
  \begin{enumerate}
  \item Poser $p \leftarrow 1$
  \item Si $n = 0$, retourner $p$
  \item Poser $p \leftarrow p \times b$
  \item Décrémenter $n$: $n \leftarrow n - 1$; retourner à l'étape 2
  \end{enumerate}
\end{frame}

\begin{frame}[plain]
  \questionbox{Comment établit-on la structure d'un algorithme? Nous
    devons certainement poser des conditions de départ, tel que la
    nature des nombres utilisés par exemple, mais y a-t-il un ordre
    précis avec lequel nous devons travailler? De plus, dans la
    structure, est ce que la syntaxe ainsi que les mots utilisés ont
    une importance? Par mon expérience avec la programmation au cégep
    avec le logiciel Maple, la syntaxe ainsi que les termes utilisés
    sont primordiaux au fonctionnement d'un algorithme. On pourrait
    aussi revenir, en rapport avec la structure de l'algorithme, sur
    les rapport de conditions-conséquences ou les
    conditions-alternatives qui me semblent, à mon avis un peu
    complexes à assimiler.}
\end{frame}

\begin{frame}
  \frametitle{Structure d'un algorithme}

  \begin{center}
    entrants
    \quad\faArrowRight\quad
    {\LARGE\faCogs}
    \quad\faArrowRight\quad
    sortants
  \end{center}
  \begin{itemize}
  \item a.k.a.\ \alert{recette} de calcul
  \item Explique \alert{quoi} faire, mais pas \alert{comment} le faire
  \item Indépendant d'un langage de programmation
  \item Ordre précis? Non, mais il y a parfois des choix meilleurs que
    d'autres
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Vocabulaire et syntaxe}

  «Par mon expérience avec la programmation au cégep avec le logiciel
  Maple, la syntaxe ainsi que les termes utilisés sont primordiaux au
  fonctionnement d'un algorithme»
  \bigskip\pause

  Foutaise. Un seul (véritable) impératif: la \alert{précision}.
  \bigskip\pause

  Visez aussi la \alert{concision}.
\end{frame}

\begin{frame}
  \frametitle{Évaluation conditionnelle}

  «On pourrait aussi revenir [...] sur les rapport de
  conditions-conséquences ou les conditions-alternatives qui me
  semblent, à mon avis un peu complexes à assimiler.»
  \bigskip\pause

  Ce n'est pas plus compliqué que du langage courant.

  \begin{quote}
    \itshape
    Si \meta{condition}, alors \meta{conséquence}

    Si \meta{condition}, alors \meta{conséquence}, sinon \meta{alternative}
  \end{quote}
\end{frame}

\begin{frame}[plain]
  \questionbox{Je reviendrais sur l’exercice 2.9 du chapitre 2 pour
    revenir sur la structure d’un algorithme et dans le but de nous
    clarifier comment bien évaluer chacun des algorithmes à la
    lettre.}
\end{frame}

\begin{frame}
  \frametitle{Algorithme B de l'exercice 2.9}

  \begin{minipage}{0.4\linewidth}
    Afficher les diviseurs d'un nombre naturel $n$.
    \begin{enumerate}[1.]
    \item Poser $i \leftarrow 1$.
    \item Si $n \mod i = 0$, afficher les valeurs de $i$ et $n/i$.
    \item Incrémenter $i$: $i \leftarrow i + 1$.
    \item Si $i < n/2$, retourner à l'étape 2.
    \end{enumerate}
  \end{minipage}
  \hfill\pause
  \begin{minipage}{0.55\linewidth}
    \centering\smaller
    $n = 16$ \medskip

    \begin{tabular}{cccl}
      \toprule
      étape & $i$ & $n \mod i$ & affichage \\
      \midrule
      \only<2>{\color{alert}}1 & \only<2>{\color{alert}}1 \\%
      \only<3>{\color{alert}}2 & \only<3>{\color{alert}}1 & \only<3>{\color{alert}}0 & \only<3>{\color{alert}}1 16 \\%
      \only<4>{\color{alert}}3 & \only<4>{\color{alert}}2 \\%
      \only<5>{\color{alert}}2 & \only<5>{\color{alert}}2 & \only<5>{\color{alert}}0 & \only<5>{\color{alert}}1 16 2 8 \\%
      \only<6>{\color{alert}}3 & \only<6>{\color{alert}}3 \\%
      \only<7>{\color{alert}}2 & \only<7>{\color{alert}}3 & \only<7>{\color{alert}}1 & \only<7>{\color{alert}}1 16 2 8 \\%
      \only<8>{\color{alert}}3 & \only<8>{\color{alert}}4 \\%
      \only<9>{\color{alert}}2 & \only<9>{\color{alert}}4 & \only<9>{\color{alert}}0 & \only<9>{\color{alert}}1 16 2 8 4 4 \\%
      \only<10>{\color{alert}}3 & \only<10>{\color{alert}}5 \\%
      \only<11>{\color{alert}}2 & \only<11>{\color{alert}}5 & \only<11>{\color{alert}}1 & \only<11>{\color{alert}}1 16 2 8 4 4 \\%
      \only<12>{\color{alert}}3 & \only<12>{\color{alert}}6 \\%
      \only<12>{\color{alert}}2 & \only<12>{\color{alert}}6 & \only<12>{\color{alert}}1 & \only<12>{\color{alert}}1 16 2 8 4 4 \\%
      \only<12>{\color{alert}}3 & \only<12>{\color{alert}}7 \\%
      \only<12>{\color{alert}}2 & \only<12>{\color{alert}}7 & \only<12>{\color{alert}}1 & \only<12>{\color{alert}}1 16 2 8 4 4 \\%
      \bottomrule
    \end{tabular}
  \end{minipage}
\end{frame}

\begin{frame}[plain]
  \questionbox{Dans le chapitre 2, les numéros 2.4 et 2.5. Je ne
    comprends pas comment faire et comment remplir les tableaux.}
\end{frame}

\begin{frame}
  \frametitle{Opérateurs booléens de base}

  \begin{minipage}[t]{0.32\linewidth}
    \centering
    $p \vee q$ \\
    vrai \alert{dès que} \\ $p$ ou $q$ est vraie \\
    \bigskip
    \begin{tabular}{ccc}
      \toprule
      $p$ & $q$ & $p \vee q$ \\
      \midrule
      V & V & V \\
      V & F & V \\
      F & V & V \\
      F & F & F \\
      \bottomrule
    \end{tabular}
  \end{minipage}
  \hfill\pause
  \begin{minipage}[t]{0.32\linewidth}
    \centering
    $p \wedge q$ \\
    vrai \alert{seulement} lorsque \\
    $p$ et $q$ sont vraies \\
    \bigskip
    \begin{tabular}{ccc}
      \toprule
      $p$ & $q$ & $p \wedge q$ \\
      \midrule
      V & V & V \\
      V & F & F \\
      F & V & F \\
      F & F & F \\
      \bottomrule
    \end{tabular}
  \end{minipage}
  \hfill\pause
  \begin{minipage}[t]{0.32\linewidth}
    \centering
    $\overline{p}$ \\
    faux lorsque $p$ est vraie, \\ et vice-versa \\
    \bigskip
    \begin{tabular}{cc}
      \toprule
      $p$ & $\overline{p}$ \\
      \midrule
      V & F \\
      F & V \\
      \bottomrule
    \end{tabular}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Propriétés importantes}

  \begin{minipage}[t]{0.35\linewidth}
    \begin{gather*}
      \overline{p \vee q} = \overline{p} \wedge \overline{q} \\
      \overline{p \wedge q} = \overline{p} \vee \overline{q}
    \end{gather*}

    \begin{gather*}
      p \rightarrow q = \overline{q} \rightarrow \overline{p} \\
      p \leftrightarrow q = \overline{p} \leftrightarrow \overline{q}
    \end{gather*}
  \end{minipage}
  \hfill
  \pause
  \begin{minipage}[t]{0.55\linewidth}
    \centering
    Exemple de démonstration
    \medskip

    $\overline{p \vee q} = \overline{p} \wedge \overline{q}$ \\
    \medskip
    \begin{tabular}{cc}
      \toprule
      $p \vee q$ & $\overline{p \vee q}$ \\
      \midrule
      V & F \\
      V & F \\
      V & F \\
      F & V \\
      \bottomrule
    \end{tabular}
    \qquad
    \begin{tabular}{ccc}
      \toprule
      $\overline{p}$ & $\overline{q}$ & $\overline{p} \wedge \overline{q}$ \\
      \midrule
      F & F & F \\
      F & V & F \\
      V & F & F \\
      V & V & V \\
      \bottomrule
    \end{tabular}
  \end{minipage}
\end{frame}

\begin{frame}[plain]
  \questionbox{Pouvez-vous expliquer l'utilisation de la commande
    \code{echo} dans le contexte de la ligne de commande Unix? Et
    comment accéder a un fichier quand on a pas la permission?}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Ohé\dots\ c'est l'écho}

  Comme son nom l'indique, la commande \code{echo} retourne simplement
  son argument.
\begin{lstlisting}
---$- echo "bonjour"
bonjour
\end{lstlisting}

  Avec le transfert de données ou la redirection, elle permet
  d'envoyer du texte à une commande ou dans un fichier.
\begin{lstlisting}
---$- echo "bonjour" | tr '[a-z]' '[A-Z]'
BONJOUR
\end{lstlisting}
\begin{lstlisting}
---$- echo "bonjour" > bonjour.txt
---$- cat bonjour.txt
bonjour
\end{lstlisting}
\end{frame}

\begin{frame}[fragile,fragile=singleslide]
  \frametitle{Permissions}

  Il faut généralement être administrateur pour modifier les
  permissions d'un fichier ou d'un répertoire (à moins d'être déjà
  propriétaire du fichier).

  \link{https://fr.wikipedia.org/wiki/Permissions_UNIX}{Permissions Unix}
\begin{Verbatim}
rwxr-xr--
 \ /\ /\ /
  v  v  v
  |  |  droits des autres utilisateurs (o)
  |  |
  |  droits des utilisateurs appartenant au groupe (g)
  |
 droits du propriétaire (u)
\end{Verbatim}
\end{frame}

\begin{frame}[plain]
  \questionbox{Lors du dernier laboratoire, nous avons vu que si le
    nom d’un document comporte des espaces entre les différentes
    parties du nom, nous remplaçons les espaces par
    «\code{\textbackslash}». Donc, est-ce qu’il y a une touche pour
    remplacer les accents sur les lettres des noms d’un document? Par
    exemple, si mon document se nomme \code{Écriture}, au lieu de
    changer le nom de mon document pour utiliser la ligne de commande
    Unix, qu’est-ce que je peux faire?}
\end{frame}

\begin{frame}
  \frametitle{Caractère d'échappement}

  \begin{itemize}[<+->]
  \item «\code{\textbackslash}» est le \alert{caractère
      d'échappement} dans une foule de langages de programmation
  \item Désactive la signification spéciale du symbole qui suit
  \item À la ligne de commande, l'espace sépare les arguments
    (signification spéciale)
  \item Désactiver pour l'utiliser comme caractère typographique:
    \begin{center}
      \code{cd Program{\textbackslash} Files}
    \end{center}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Lettres accentuées}

  Rien de spécial à faire\dots\ sinon que certains programmes n'aiment
  pas ça.
  \bigskip\pause

  (Raison: Windows n'utilise toujours pas le standard international
  UTF-8 en vigueur partout ailleurs depuis au moins 20 ans.)
\end{frame}

\begin{frame}[plain]
  \questionbox{Concernant la validation avec Roger: j’ai tenté de
    copier le dépôt test fourni dans la section activité
    complémentaire du contenu de la semaine 3, mais le message
    d’erreur suivant survient: \code{ fatal: impossible d'accéder à
      'https://projets.fsg.ulaval.ca/git/[...]/': Failed to connect to
      projets.fsg.ulaval.ca port 443 after 75013 ms: Couldn't connect
      to server}}
\end{frame}

\begin{frame}
  \frametitle{Deux choses}

  \textbf{Sur la validation avec Roger} \\
  RTFM
  \bigskip\pause

  \textbf{Sur l'accès à Bitbucket} \\
  Utiliser le VPN depuis l'extérieur du campus
\end{frame}

\begin{frame}[plain]
  \profbox{Je vous propose d'étudier brièvement un langage de
    programmation unique et original: le langage utilisé par
    \textsc{Bib}{\TeX} pour préparer des entrées de bibliographie à
    partir d'une base de données.}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Fonction \textsc{Bib}{\TeX}}

  Mise en forme d'un
  \link{https://fr.wikipedia.org/wiki/Digital_Object_Identifier}{DOI}
  ou d'une URL.

  \begin{minipage}{0.5\linewidth}
\begin{lstlisting}[basicstyle=\small\ttfamily\NoAutoSpacing]
FUNCTION {format.url}
{
  `\alt<4>{\alert{doi}}{doi}'
  `\alt<5>{\alert{empty\$}}{empty\$}'
    `\alt<6>{\alert{\{ url \}}}{\{ url \}}'
    `\alt<6>{\alert{\{ "DOI:" doi * \}}}{\{ "DOI:" doi * \}}'
  `\alt<6>{\alert{if\$}}{if\$}'
  `\alt<7>{\alert{duplicate\$}}{duplicate\$}' `\alt<8>{\alert{empty\$}}{empty\$}'
    { pop$ "" }
    { `\alt<9>{\alert{"URL \textbackslash{}url\{"}}{"URL {\textbackslash}url\{"}' `\alt<10>{\alert{swap\$}}{swap\$}' `\alt<11>{\alert{*}}{*}' `\alt<12>{\alert{"\}"}}{"\}"}' `\alt<13>{\alert{*}}{*}' }
  if$
}
\end{lstlisting}
  \end{minipage}
  \hfill
  \only<2>{
    \begin{minipage}{0.35\linewidth}
      Caractéristiques du langage
      \begin{itemize}
      \item pile de type \emph{last in first out} (LIFO)
      \item suffixé
      \item \code{empty\$}, \code{if\$}, \code{*}, \dots, sont des
        opérateurs
      \end{itemize}
    \end{minipage}
    \hfill
    \begin{minipage}{0.12\linewidth}
      \href{https://commons.wikimedia.org/w/index.php?curid=67631269}{%
        \includegraphics[width=\linewidth,keepaspectratio]{images/Tallrik_-_Ystad-2018}} \\
    \end{minipage}
  }
  \only<3->{
    \begin{minipage}{0.47\linewidth}
      \begin{minipage}{\linewidth}
        arguments \\
        \begin{tabular}{l}
          \code{doi = ""} \\
          \code{url = "https://foo.org"}
        \end{tabular}
      \end{minipage}
      \bigskip
      \begin{minipage}[t][0.3\textheight][c]{\linewidth}
        pile \\
        \setlength{\arrayrulewidth}{1pt}
        \begin{tabularx}{17em}{|X|}
          \hline
          \only<4>{\texttt{""}}
          \only<5>{\texttt{1}}
          \only<6>{\texttt{https://foo.org}}
          \only<7>{\texttt{https://foo.org} \\ \hline \texttt{https://foo.org}}
          \only<8>{\texttt{0} \\ \hline \texttt{https://foo.org}}
          \only<9>{\texttt{URL {\textbackslash}url\{} \\ \hline \texttt{https://foo.org}}
          \only<10>{\texttt{https://foo.org} \\ \hline \texttt{URL {\textbackslash}url\{}}
          \only<11>{\texttt{URL {\textbackslash}url\{https://foo.org}}
          \only<12>{\texttt{\}} \\ \hline \texttt{URL {\textbackslash}url\{https://foo.org}}
          \only<13>{\texttt{URL {\textbackslash}url\{https://foo.org\}}} \\
          \hline
        \end{tabularx}
      \end{minipage}
      \begin{minipage}{\linewidth}
        résultat \\
        \uncover<14>{\texttt{URL {\textbackslash}url\{https://foo.org\}}}
      \end{minipage}
    \end{minipage}
  }
\end{frame}

% \begin{frame}[plain]
%   \questionbox{Pouvez-vous expliquer comment utiliser les caractères
%     de substitution \newline (\code{*}, \code{?}, \code{[]}) avec la commande
%     \code{ls}?}
% \end{frame}

% \begin{frame}[fragile]
%   \frametitle{Métacaractères de la ligne de commande Unix}

%   \begin{minipage}[t]{0.55\linewidth}
%     Les caractères \code{*}, \code{?} et \code{[]} sont les principaux
%     opérateurs des
%     \link{https://en.wikipedia.org/wiki/Glob_(programming)}{motifs
%       \emph{glob}}.
%     \medskip

%     \begin{tabularx}{\linewidth}{cX}
%       \code{*} & tout caractère, 0 ou plusieurs fois \\
%       \code{?} & tout caractère, 1 seule fois \\
%       \code{[abc]} & un caractère parmi la liste \\
%       \code{[a-z]} & un caractère dans la plage \\
%     \end{tabularx}
%   \end{minipage}
%   \hfill\pause
%   \begin{minipage}[t]{0.40\linewidth}
%     Tous les fichiers
% \begin{lstlisting}
% ---$- ls *
% \end{lstlisting}
%     Tous les fichiers avec une extension d'un seul caractère
% \begin{lstlisting}
% ---$- ls *.?
% \end{lstlisting}
%     Tous les fichiers débutant par un ou plusieurs chiffres
% \begin{lstlisting}
% ---$- ls [0-9]*
% \end{lstlisting}
%   \end{minipage}
% \end{frame}

% \begin{frame}[plain]
%   \questionbox{En lien avec l'exercice 3.5 du document de référence,
%     si nous utilisons un éditeur de code (par exemple VS~Code) au lieu
%     d'un IDE, allons-nous avoir de la difficulté à suivre les
%     prochains chapitres? Bref, est-ce que l'utilisation d'un IDE est
%     recommandé pour ce cours? Quels sont les avantages de Emacs par
%     rapport à VS~Code?}
% \end{frame}

% \begin{frame}
%   \frametitle{RStudio vs VS~Code vs GNU Emacs}

%   «Utilisez un seul éditeur et utilisez-le bien. Votre éditeur devrait
%   devenir une extension de votre main ; assurez-vous qu’il est confi-
%   gurable, extensible et programmable.» (Hunt et Thomas, 1999)

%   \begin{itemize}
%   \item RStudio est un environnement de développement intégré (IDE),
%     mais surtout adapté à R (et aux technologies de Posit)
%   \item RStudio est un peu devenu un standard \emph{de facto}
%   \item VS~Code et Emacs sont des éditeurs beaucoup plus puissants que
%     RStudio
%   \item Emacs est un logiciel libre qui n'essaiera pas de vous vendre
%     quoi que ce soit
%   \end{itemize}
% \end{frame}

% \begin{frame}[fragile]
%   \frametitle{Algorithme 2.7}

%   Basé sur
%   \begin{equation*}
%     b^n = b \times b^{n - 1}, \quad b^0 = 1.
%   \end{equation*}

%   Élever un nombre réel $b$ à la puissance entière positive $n$.
% \begin{Verbatim}[commandchars=\\\{\}]
% Puissance(nombre réel b, entier positif n)
%   Si (n = 0)
%     Retourner 1
%   Retourner b \times Puissance(b, n - 1)
% Fin Puissance
% \end{Verbatim}
% \end{frame}

% \begin{frame}[fragile]
%   \frametitle{Version itérative (langage naturel)}

%   Basé plutôt sur
%   \begin{equation*}
%     b^n = b \times b \times \cdots \times b.
%   \end{equation*}

%   Élever un nombre réel $b$ à la puissance entière positive $n$.
%   \begin{enumerate}
%   \item Poser $p \leftarrow 1$
%   \item Si $n = 0$, retourner $p$
%   \item Poser $p \leftarrow p \times b$
%   \item Décrémenter $n$: $n \leftarrow n - 1$; retourner à l'étape 2
%   \end{enumerate}
% \end{frame}

% \begin{frame}[fragile]
%   \frametitle{Version itérative (pseudocode)}

%   Élever un nombre réel $b$ à la puissance entière positive $n$.
% \begin{Verbatim}[commandchars=\\\{\}]
% Puissance(nombre réel b, entier positif n)
%     p \leftarrow 1
%     Tant que n > 0
%         p \leftarrow p \times b
%         n \leftarrow n - 1
%     Fin tant que
%     Retourner p
% Fin Puissance
% \end{Verbatim}
% \end{frame}

% \begin{frame}[fragile]
%   \frametitle{Algorithme 2.7a}

%   Réduire le nombre d'opérations en tirant profit du fait que
%   \begin{equation*}
%     b^{21} = b \times ((b \times (b^2)^2)^2)^2
%   \end{equation*}

%   Élever un nombre réel $b$ à la puissance entière positive $n$.
% \begin{Verbatim}[commandchars=\\\{\}]
% Puissance(nombre réel b, entier positif n)
%   Si (n = 0)
%     Retourner 1
%   Si (n est pair)
%     Retourner (Puissance(b, n/2))^2
%   Si (n est impair)
%    Retourner b \times Puissance(b, n - 1)
% Fin Puissance\end{Verbatim}
% \end{frame}

% \begin{frame}[fragile]
%   \frametitle{Version itérative (langage naturel)}

%   \begin{minipage}{0.7\linewidth}
%     Élever un nombre réel $b$ à la puissance entière positive $n$.
%     \begin{enumerate}
%     \item Poser $p \leftarrow 1$
%     \item Si $n = 0$, retourner $p$
%     \item Si $n$ est pair, poser $p \leftarrow p \times p$ et
%       $n \leftarrow n/2$; retourner à l'étape 2
%     \item Si est impair, poser $p \leftarrow p \times b$ et
%       $n \leftarrow n - 1$; retourner à l'étape 2
%     \end{enumerate}
%   \end{minipage}
%   \hfill
%   \pause
%   \begin{minipage}{0.25\linewidth}
%     \begin{tabular}{ccc}
%       \toprule
%       étape & $p$ & $n$ \\
%       \midrule
%       1 & $1$ & $21$ \\
%       4 & $b$ & $20$ \\
%       3 & $b^2$ & $10$ \\
%       3 & $b^4$ & $5$ \\
%       4 & $b^5$ & $4$ \\
%       3 & $b^{10}$ & $2$ \\
%       3 & $b^{20}$ & $1$ \\
%       4 & $b^{21}$ & $0$ \\
%       2 & $b^{21}$ \\
%       \bottomrule
%     \end{tabular}
%   \end{minipage}
% \end{frame}


%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "classes-maitre"
%%% coding: utf-8
%%% End:
