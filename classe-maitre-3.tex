%%% Copyright (C) 2023-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet «IFT-[47]902 Classes de maitre»
%%% https://gitlab.com/vigou3/ift-4902-classes-maitre
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\section{Classe de maitre 3}

\begin{frame}[plain]
  \questionbox{J'aimerais savoir quelles sont les avantages d'utiliser
    des fonctions d'application par rapport aux boucles classiques en
    termes de lisibilité et de performance? De plus, dans quels cas
    spécifiques une boucle classique pourrait-elle être préférable à
    une fonction d'application?}
\end{frame}

\begin{frame}
  \begin{description}
  \item[sucre syntaxique] n.~m.\ Fonctionnalités appliquées aux
    langages de programmation pour rendre leur lecture plus facile.
  \end{description}
\end{frame}

\begin{frame}[plain]
  Les fonctions d'application sont du sucre syntaxique.

  Les boucles \code{for}, \code{while}, \code{repeat} sont
  \alert{équivalentes} et \alert{interchangeables}.

  Le fait que les trois existent est du sucre syntaxique.
\end{frame}

\begin{frame}[fragile]
  \frametitle{Application vs boucles}

  \textbf{Comparer}

  \begin{minipage}[t]{0.45\linewidth}
    \begin{Schunk}
\begin{Verbatim}
z <- apply(x, 1, f)
\end{Verbatim}
    \end{Schunk}
  \end{minipage}
  \hfill
  \begin{minipage}[t]{0.45\linewidth}
    \begin{Schunk}
\begin{Verbatim}
n <- nrow(x)
z <- numeric(n)
for (i in seq_len(n))
{
    z[i] <- f(x[i, ])
}
\end{Verbatim}
    \end{Schunk}
  \end{minipage}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Deux grands types de boucles}

  \begin{enumerate}
  \item \textbf{À dénombrement} --- \alert{nombre} de répétitions connu
    d'avance
    \begin{Schunk}
\begin{Verbatim}
for (i in 1:10)
    x[i] <- ...
\end{Verbatim}
    \end{Schunk}
  \item \textbf{À condition} --- nombre de répétitions inconnu, mais
    \alert{condition} d'arrêt connue \\
    \medskip
    \begin{minipage}[t]{0.45\linewidth}
      boucle exécutée 0 ou plusieurs fois
      \begin{Schunk}
\begin{Verbatim}
while (s <= 1)
    ...
\end{Verbatim}
      \end{Schunk}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{0.45\linewidth}
      boucle exécutée au moins une fois
      \begin{Schunk}
\begin{Verbatim}
repeat
{
    ...
    if (s > 1)
        break
}
\end{Verbatim}
      \end{Schunk}
    \end{minipage}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Formulations fonctionnellement équivalentes}

  Quelle version est la plus facile à consulter?

  \begin{minipage}{0.45\linewidth}
    \begin{Schunk}
\begin{Verbatim}
for (j in seq_len(n))
{
    res[j] <- x[j] - y[j] + r
}
\end{Verbatim}
    \end{Schunk}
    \pause

    \begin{Schunk}
\begin{Verbatim}
j <- 1
while (j <= n)
{
    res[j] <- x[j] - y[j] + r
    j <- j + 1
}
\end{Verbatim}
    \end{Schunk}
  \end{minipage}
  \hfill\pause
  \begin{minipage}{0.45\linewidth}
    \begin{Schunk}
\begin{Verbatim}
j <- 1
repeat
{
    res[j] <- x[j] - y[j] + r
    j <- j + 1
    if (j == n)
        break
}
\end{Verbatim}
    \end{Schunk}
  \end{minipage}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Choisir entre \code{while} et \code{repeat}}

  Si vous devez forcer la première exécution d'une boucle \code{while}
  avec quelque chose comme
  \begin{Schunk}
\begin{Verbatim}
s <- 0
while (s <= 1)
    ...
\end{Verbatim}
  \end{Schunk}
  \dots\ c'est que vous avez besoin d'une boucle \code{repeat}!
\end{frame}

\begin{frame}[plain]
  \questionbox{Lorsque j’ai appris à programmer en
    C\raisebox{-.125ex}{\larger+}\raisebox{-.125ex}{\larger+}, on m’a
    conseillé de privilégier la boucle \code{for}, car une boucle
    \code{while} mal maîtrisée pourrait entraîner une exécution
    infinie et faire planter l'ordi. Est-ce également le cas en R?}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{\texttt{<perplexe>}}

  Les structures valides suivantes causent toutes des boucles infinies
  dans R.

  \begin{minipage}[t]{0.32\linewidth}
    \begin{Schunk}
\begin{Verbatim}
for ()
{
    ...
}
\end{Verbatim}
    \end{Schunk}
  \end{minipage}
  \hfill
    \begin{minipage}[t]{0.32\linewidth}
    \begin{Schunk}
\begin{Verbatim}
while ()
{
    ...
}
\end{Verbatim}
    \end{Schunk}
  \end{minipage}
  \hfill
  \begin{minipage}[t]{0.32\linewidth}
    \begin{Schunk}
\begin{Verbatim}
repeat
{
    ...
}
\end{Verbatim}
    \end{Schunk}
  \end{minipage}
\end{frame}

\begin{frame}[plain]
  \questionbox{J'ai de la difficulté à bien comprendre le lien entre
    les fonctions internes \code{sort}, \code{rank} et \code{order} et
    comment bien les utiliser pour trier des données. Je ne suis pas
    certain de la différence entre les trois et de bien interpréter
    les données que ces fonctions renvoient dans R. Par exemple, dans
    l'exercice 7.4, je ne comprends pas trop le lien entre
    \code{rank}, \code{order} et le tri par dénombrement.}
\end{frame}

\def\bdn#1{\scsnowman[adjustbaseline,scale=#1,buttons,nose=orange]}
\begin{frame}[plain]
  \large\centering
  \begin{tabular}{lccc}
    & \bdn{5} & \bdn{7} & \bdn{3} \\
    \code{sort}  \onslide<2->{& \bdn{1.00} & \bdn{1.67} & \bdn{2.33}} \\
    \code{rank}  \onslide<3->{& 2          & 3          & 1} \\
    \code{order} \onslide<4->{& 3          & 1          & 2}  \\
  \end{tabular}
\end{frame}

\begin{frame}[plain]
  \questionbox{J'ai de la difficulté à comprendre l'utilité des tests
    unitaires. Je comprends les tests eux-mêmes, mais pourquoi
    utiliser une fonction pour détecter les erreurs au lieu d'éxécuter
    le code et observer s'il y a une erreur?}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Regardons un cas «connu»}

  \begin{minipage}{0.55\linewidth}
    \begin{Schunk}
\begin{Verbatim}[fontsize=\footnotesize]
> id2dirname("555", "_foo")
[1] "555_foo"
> id2dirname(c(555, 42), "_foo")
[1] "555_foo" "42_foo"
> id2dirname(555, "bar", prefix = "foo/")
[1] "foo/555bar"
> id2dirname(c("555", "42"), "bar", "foo/")
[1] "foo/555bar" "foo/42bar"
\end{Verbatim}
    \end{Schunk}
  \end{minipage}
  \pause

  \hfill\rotatebox{180}{\larger[2]\faReply}\hfill
  \begin{minipage}[t]{0.82\linewidth}
    \begin{Schunk}
\begin{Verbatim}[fontsize=\footnotesize]
> stopifnot(identical("555_foo",
                      id2dirname("555", "_foo")))
> stopifnot(identical(c("555_foo", "42_foo"),
                      id2dirname(c(555, 42), "_foo")))
> stopifnot(identical("foo/555bar",
                      id2dirname(555, "bar", prefix = "foo/")))
> stopifnot(identical(c("foo/555bar", "foo/42bar"),
                      id2dirname(c("555", "42"), "bar", "foo/")))
\end{Verbatim}
    \end{Schunk}
  \end{minipage}
\end{frame}

\begin{frame}[plain]
  \profbox{Dans quels fichiers d'exemples du document de référence
    retrouve-t-on \meta{motif}?}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{C'est un boulot pour \code{grep}}

  \begin{enumerate}
  \item Toutes les utilisations de \code{matrix}
    \begin{Schunk}
\begin{Verbatim}
$ pwd
/Users/vincent/Desktop/programmer-avec-r
$ grep matrix *.R
\end{Verbatim}
    \end{Schunk}
  \item Fichiers contenant au moins une utilisation de \code{matrix}
    \begin{Schunk}
\begin{Verbatim}
$ grep -l matrix *.R
\end{Verbatim}
    \end{Schunk}
  \item Utilisations de l'opérateur \code{\%o\%}
    \begin{Schunk}
\begin{Verbatim}
$ grep '%o%' *.R
\end{Verbatim}
    \end{Schunk}
  \item Utilisations de l'opérateur \code{\%*\%}
    \begin{Schunk}
\begin{Verbatim}
$ grep '%\*%' *.R
\end{Verbatim}
    \end{Schunk}
  \end{enumerate}
\end{frame}

\begin{frame}[plain]
  \profbox{Comment extraire uniquement les noms des stations BIXI de
    l'année 2021?}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Plusieurs solutions possibles}

  Avec \code{awk} en changeant le délimiteur de champs.
  \begin{Schunk}
\begin{Verbatim}
$ awk -F ',' '{ print $2 }' Stations_2021u.csv
\end{Verbatim}
  \end{Schunk}

  Avec \code{sed} (pas très naturel).
  \begin{Schunk}
\begin{Verbatim}
$ sed -E 's/[^,]*,([^,]*),.*/\1/' Stations_2021u.csv
\end{Verbatim}
  \end{Schunk}

  Avec l'utilitaire \code{cut} mentionné dans le laboratoire
  \emph{Ligne de commande Unix}!
  \begin{Schunk}
\begin{Verbatim}
$ cut -d, -f2 Stations_2021u.csv
\end{Verbatim}
  \end{Schunk}
\end{frame}

\begin{frame}[plain]
  \profbox{Combien y a-t-il eu de locations BIXI au départ de la
    station au coin de Berri et Rachel en juin 2021? Pour toute
    l'année 2021?}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{D'abord des solutions avec \code{grep}}

  Trouver le code de la station.
  \begin{Schunk}
\begin{Verbatim}
$ grep 'Berri / Rachel' Stations_2021u.csv
\end{Verbatim}
  \end{Schunk}

  Identifier les codes dans la \alert{deuxième} colonne des données
  d'historique des locations.
  \begin{Schunk}
\begin{Verbatim}
$ grep -E -c '^[^,]*,252' OD_2021-06u.csv
$ grep -E -c '^[^,]*,252' OD_2021-*u.csv
\end{Verbatim}
  \end{Schunk}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Plus facile à réaliser avec \code{awk}}

  Utiliser \code{awk} uniquement pour trouver les lignes.
  \begin{Schunk}
\begin{Verbatim}
$ awk -F ',' '$2 == 252' OD_2021-06u.csv | wc -l
$ awk -F ',' '$2 == 252' OD_2021-*u.csv | wc -l
\end{Verbatim}
  \end{Schunk}

  Autre manière de changer le séparateur de champs.
  \begin{Schunk}
\begin{Verbatim}
$ awk 'BEGIN { FS = "," } $2 == 252' OD_2021-06u.csv | wc -l
$ awk 'BEGIN { FS = "," } $2 == 252' OD_2021-*u.csv | wc -l
\end{Verbatim}
  \end{Schunk}

  Compter les lignes avec \code{awk}.
  \begin{Schunk}
\begin{Verbatim}
$ awk -F ',' '$2 == 252 { i++ } END { print i }' OD_2021-06u.csv
$ awk -F ',' '$2 == 252 { i++ } END { print i }' OD_2021-*u.csv
\end{Verbatim}
  \end{Schunk}
\end{frame}

\begin{frame}[plain]
  \profbox{Quelles ont été les stations d'arrivée des locations BIXI
    qui ont débuté à la station au coin de Berri et Rachel en juin
    2021?}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{C'est vraiment plus simple avec \code{awk}}

  Liste des stations d'arrivée pour toutes les locations.
  \begin{Schunk}
\begin{Verbatim}
$ awk -F ',' '$2 == 252 { print $4 }' OD_2021-06u.csv
\end{Verbatim}
  \end{Schunk}

  Liste des stations d'arrivée \alert{différentes}.
  \begin{Schunk}
\begin{Verbatim}
$ awk -F ',' '$2 == 252 { print $4 }' OD_2021-06u.csv | sort | uniq
$ awk -F ',' '$2 == 252 { print $4 }' OD_2021-06u.csv | sort -u
\end{Verbatim}
  \end{Schunk}
\end{frame}

\begin{frame}[plain]
  \questionbox{Serait-il possible de revenir sur l'exercice 11.2. En
    particulier les parties e), f), g) et h).}
\end{frame}

\begin{frame}
  \frametitle{Exercice 11.2}

  \begin{columns}
    \begin{column}{0.50\textwidth}
      \begin{enumerate}[a)]
        \setcounter{enumi}{4}
      \item \code{[a-z]+[.?!]} \\
        \onslide<2->{lettre minuscule} \\
        \onslide<3->{une lettre minuscule ou plus} \\
        \onslide<4->{l'un ou l'autre des symboles \code{.}, \code{?}, \code{!}}
      \end{enumerate}
    \end{column}
    \begin{column}{0.45\textwidth}
      \begin{enumerate}[1.]
      \item \texttt{\makealert<2->{b}\makealert<3->{attle}\makealert<4->{!}}
      \item \texttt{Hot}
      \item \texttt{\makealert<2-3>{g}\makealert<3-3>{reen}}
      \item \texttt{\makealert<2->{s}\makealert<3->{wamping}\makealert<4->{.}}
      \item \texttt{\makealert<2-3>{j}\makealert<3-3>{ump}~up.}
      \item \texttt{\makealert<2->{u}\makealert<3->{ndulate}\makealert<4->{?}}
      \item \texttt{\makealert<2-3>{i}\makealert<3-3>{s}.?}
      \end{enumerate}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Exercice 11.2}

  \begin{columns}
    \begin{column}{0.50\textwidth}
      \begin{enumerate}[a)]
        \setcounter{enumi}{5}
      \item \code{[a-zA-Z]*[\string^,]=} \\
        \onslide<2->{lettre minucule ou majuscule} \\
        \onslide<3->{0 ou plusieurs lettres} \\
        \onslide<4->{tout caractère autre qu'une virgule} \\
        \onslide<5->{symbole \code{=}}
      \end{enumerate}
    \end{column}
    \begin{column}{0.45\textwidth}
      \begin{enumerate}[1.]
      \item \texttt{\makealert<2->{B}\makealert<3->{utt}\makealert<5->{=}}
      \item \texttt{\makealert<2-3>{B}\makealert<3-3>{otHEr},=}
      \item \texttt{\makealert<2-3>{A}\makealert<3-3>{mple}}
      \item \texttt{\makealert<2-4>{F}\makealert<3-4>{IdDlE}\makealert<4-4>{7}h=}
      \item \texttt{\makealert<2->{B}\makealert<3->{rittle}\makealert<4->{~}\makealert<5->{=}}
      \item \texttt{\makealert<2->{O}\makealert<3->{ther}\makealert<4->{.}\makealert<5->{=}}
      \end{enumerate}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Exercice 11.2}

  \begin{columns}
    \begin{column}{0.50\textwidth}
      \begin{enumerate}[a)]
        \setcounter{enumi}{6}
      \item \code{[a-z][.?!][[:blank:]]+[A-Z]} \\
        \onslide<2->{lettre minuscule} \\
        \onslide<3->{l'un ou l'autre des symboles \code{.}, \code{?}, \code{!}} \\
        \onslide<4->{un blanc (espace, tabulation)} \\
        \onslide<5->{un blanc ou plus} \\
        \onslide<6->{lettre majuscule}
      \end{enumerate}
    \end{column}
    \begin{column}{0.45\textwidth}
      \begin{enumerate}[1.]
      \item \texttt{A.~B}
      \item \texttt{\makealert<2-5>{c}\makealert<3-5>{!}~d}
      \item \texttt{\makealert<2-2>{e}~f}
      \item \texttt{\makealert<2->{g}\makealert<3->{.}~\makealert<6->{H}}
      \item \texttt{\makealert<2->{i}\makealert<3->{?}~\makealert<6->{J}}
      \item \texttt{\makealert<2-2>{k}~L}
      \end{enumerate}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Exercice 11.2}

  \begin{columns}
    \begin{column}{0.50\textwidth}
      \begin{enumerate}[a)]
        \setcounter{enumi}{7}
      \item \code{<[\string^>]+>} \\
        \onslide<2->{symbole \code{<}} \\
        \onslide<3->{tout symbole autre que \code{>}} \\
        \onslide<4->{un symbole autre que \code{>} ou plus} \\
        \onslide<5->{symbole \code{>}}
      \end{enumerate}
    \end{column}
    \begin{column}{0.45\textwidth}
      \begin{enumerate}[1.]
      \item \texttt{\makealert<2->{<}\makealert<3->{a}\makealert<4->{n~xml~tag}\makealert<5->{>}}
      \item \texttt{\makealert<2-4>{<}\makealert<3-4>{o}\makealert<4-4>{pentag}>~<closetag>}
      \item \texttt{\makealert<2->{<}\makealert<3->{/}\makealert<4->{closetag}\makealert<5->{>}}
      \item \texttt{\makealert<2-2>{<}>}
      \item \texttt{\makealert<2->{<}\makealert<3->{w}\makealert<4->{ith~attribute=”77”}\makealert<5->{>}}
      \end{enumerate}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \questionbox{Dans le chapitre~11, j'ai de la difficulté à lire le
    language des expressions régulières car je n'arrive pas à
    déterminer ce que certains caractères possèdent comme impact.
    Certains semblent affecter ce qui se trouve sur leur droite,
    d'autres sur leur gauche, un certain groupe, certains possèdent
    une signification précise, etc.}
\end{frame}

\begin{frame}
  \frametitle{Soupe à l'alphabet}

  \begin{itemize}
  \item Expressions régulières est un langage de programmation qui
    n'utilise (presque) pas de mots clés
  \item Nombre de symboles sur le clavier limité
  \item Certains symboles ont plusieurs significations
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Question spécifique sur \code{grep}}

  \begin{columns}[t]
    \begin{column}{0.50\textwidth}
      \code{grep '\string^[\string^\#]*[slt]apply'} \\
      \onslide<2->{début de ligne} \\
      \onslide<3->{tout symbole autre que \code{\#}} \\
      \onslide<4->{0 ou plusieurs symboles \code{\#}} \\
      \onslide<5->{l'une ou l'autre de \code{s}, \code{l}, \code{t}} \\
      \onslide<6->{le mot \code{apply}}
    \end{column}
    \begin{column}{0.45\textwidth}
      \onslide<7->{«lignes qui débutent par}
      \onslide<7->{autre chose que}
      \onslide<7->{des symboles de commentaire}
      \onslide<7->{et qui contiennent \code{sapply}, \code{lapply} ou \code{tapply}»}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Question spécifique sur \code{sed}}

  \begin{columns}[t]
    \begin{column}{0.50\textwidth}
      \code{sed 's/\string\./,/'} \\
      \onslide<2->{rechercher} \\
      \onslide<3->{un point} \\
      \onslide<4->{remplacer par une virgule} \\
    \end{column}
    \begin{column}{0.45\textwidth}
      \onslide<5->{«rechercher le premier point sur une ligne et le
        remplacer par une virgule»}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Question spécifique sur \code{awk}}

  \begin{minipage}{0.7\textwidth}
    \code{awk '/\string^\$/ \&\& state == 1 \{ exit \}} \\
    \verb+     +\code{/\string^\$/ \{ state = 1 \}} \\
    \verb+     +\code{1'} \\
    \onslide<2->{lorsqu'une ligne est vide et que \code{state} vaut
      1, terminer} \\
    \onslide<3->{lorsqu'une ligne est vide, poser \code{state} égal
      à 1} \\
    \onslide<4->{afficher le contenu de la ligne} \\
  \end{minipage} \\
  \hfill
  \begin{minipage}{0.45\textwidth}
    \onslide<5->{«afficher le contenu du fichier jusqu'à la deuxième
      ligne blanche»}
  \end{minipage}
\end{frame}

\begin{frame}[plain]
  \questionbox{Si \code{awk} peut effectuer une multitude de
    traitements sur des fichiers texte, pourquoi on ne l'utilise pas
    systématiquement pour tous les types de manipulation de données? }
\end{frame}

\begin{frame}
  \frametitle{Il y a plusieurs outils pour découper une planche}

  \begin{itemize}
  \item \code{awk} peut souvent réaliser les mêmes choses que
    \code{grep} ou \code{sed}
  \item Utiliser l'outil le plus naturel pour la tâche
  \item La plupart des langages de programmation (dont R) contiennent
    une fonction \code{grep}
  \item Dans plusieurs langages de programmation (dont R), la fonction
    pour rechercher et remplacer est \code{sub} (et \code{gsub})
  \item Pas d'équivalent de \code{awk} dans la plupart des langages de
    programmation
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \questionbox{Pourriez-vous revenir sur l'utilité de {\LaTeX} et
    pourquoi ce logiciel était seulement nécessaire dans le cours
    Méthodes numériques en actuariat?}
\end{frame}

\begin{frame}
  \frametitle{[OT]}

  \begin{itemize}
  \item {\LaTeX} est un système de mise en page pour produire des
    documents de très grande qualité typographique
  \item Requis pour produire la version PDF des rubriques d'aide de R
  \item Aussi utilisé par le système R~Markdown
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \questionbox{Je me questionnais sur le fonctionnement d'un VPN
    (pourquoi en as ton besoin dans le cadre du cours) et quels sont
    les risques d'oublier celui-ci ouvert sur notre ordinateur?}
\end{frame}

\begin{frame}
  \frametitle{[OT]}

  \begin{itemize}
  \item Un VPN redirige le trafic entre votre ordinateur et un site
    externe
    \begin{center}
      \larger[2]
      \def\updown{\faLongArrowAltDown\faLongArrowAltUp}
      \begin{tabular}{cp{10mm}cc}
        {\small sans VPN} & & {\small avec VPN} \\
        \faCloud & & \faCloud \makebox[2mm][l]{\raisebox{-7mm}{\rotatebox{45}{\updown}}} \\
        \updown  & & & \faServer \\
        \faLaptop & & \faLaptop \makebox[2mm][l]{\raisebox{7mm}{\rotatebox{-45}{\updown}}}
      \end{tabular}
    \end{center}
  \item Le site externe ne voit pas votre véritable adresse
  \end{itemize}
\end{frame}

% \begin{frame}[plain]
%   \questionbox{Concernant les boucles à dénombrement, comment
%     pouvons-nous itérer sur une matrice ou un tableau? Autrement dit,
%     comment répéter une procédure pour chaque élément?}
% \end{frame}

% \begin{frame}[fragile=singleslide]
%   \frametitle{Rien de spécial}

%   \begin{enumerate}
%   \item \code{apply} est là pour ça
%   \item Si ce n'est pas une option simple \par\medskip
%     \begin{minipage}{0.475\linewidth}
%       \begin{Schunk}
% \begin{Verbatim}
% for (i in seq_len(nrow(x)))
%     x[i, ] <- ...
% \end{Verbatim}
%       \end{Schunk}
%     \end{minipage}
%     \hfill
%     \begin{minipage}{0.475\linewidth}
%       \begin{Schunk}
% \begin{Verbatim}
% for (j in seq_len(ncol(x)))
%     x[, j] <- ...
% \end{Verbatim}
%       \end{Schunk}
%     \end{minipage}
%   \end{enumerate}
% \end{frame}

% \begin{frame}[plain]
%   \questionbox{J'ai écouté la capsule de révision sur les boucles
%     itératives et je me questionne à propos de la mise en œuvre de
%     l'algorithme pour le tri par sélection. Je comprends où on veut en
%     venir avec cet algorithme et je comprends l'exemple. Cependant, la
%     mise en œuvre de cet algorithme en R me cause de la confusion. La
%     multitude d'accolades et de boucles à dénombrement m'embrouille.
%     Je ne sais pas le moment où l'on sort de ces accolades afin de
%     poursuivre les expressions suivantes sachant qu’il n’y a pas de
%     \code{break}.}
% \end{frame}

% \begin{frame}[plain]
%   \begin{textblock*}{\paperwidth}(0mm,0mm)
%     \includegraphics[width=160mm,keepaspectratio]{images/main-cartes}
%   \end{textblock*}

%   \begin{textblock*}{\paperwidth}(0mm,40mm)
%     \pgfsetfillopacity{0.6}
%     \textcolor{white}{\rule{\linewidth}{10mm}}
%     \pgfsetfillopacity{1}
%   \end{textblock*}

%   \begin{textblock*}{\paperwidth}(0mm,40mm)
%     \video{https://youtu.be/14c5N8CfnUo?t=0m43s}{Illustration des algorithmes de tri}
%   \end{textblock*}
% \end{frame}

% \begin{frame}[fragile]
%   \frametitle{Analyse d'une mise en œuvre}

%   \begin{minipage}{0.48\linewidth}
% \begin{lstlisting}[basicstyle=\footnotesize\ttfamily\NoAutoSpacing]
% `{\only<2>{\color{alert}}Selectionsort(Data: values[])}'
%   `{\only<4>{\color{alert}}For i = 0 To <length of values> - 1}'
%     `{\only<5>{\color{alert}}<Find the smallest item}'
%      `{\only<5>{\color{alert}}with index j >= i.>}'
%     `{\only<6>{\color{alert}}<Swap values[i] and values[j].>}'
%   `{\only<4>{\color{alert}}{}Next i}'
% `{\only<2>{\color{alert}}End Selectionsort}'
% \end{lstlisting}
%   \end{minipage}
%   \hfill
%   \begin{minipage}{0.48\linewidth}
% \begin{lstlisting}[basicstyle=\footnotesize\ttfamily\NoAutoSpacing]
% `{\only<2>{\color{alert}}selectionsort <- function(x)}'
% `{\only<2>{\color{alert}}\{}'
%   `{\only<3>{\color{alert}}xlen <- length(x)}'
%   `{\only<4>{\color{alert}}for (i in seq\_len(xlen))}'
%   `{\only<4>{\color{alert}}\{}'
%     `{\only<5>{\color{alert}}i.min <- i}'
%     `{\only<5>{\color{alert}}for (j in i:xlen)}'
%     `{\only<5>{\color{alert}}\{}'
%       `{\only<5>{\color{alert}}if (x[j] < x[i.min])}'
%         `{\only<5>{\color{alert}}i.min <- j}'
%     `{\only<5>{\color{alert}}\}}'
%     `{\only<6>{\color{alert}}x[c(i, i.min)] <- x[c(i.min, i)]}'
%   `{\only<4>{\color{alert}}\}}'
%   `{\only<2>{\color{alert}}x}'
% `{\only<2>{\color{alert}}\}}'
% \end{lstlisting}
%   \end{minipage}

%   \onslide<6>
%   \begin{center}
%     \textbf{Quiz express}\quad
%     Identifiez les accolades optionnelles dans le code.
%   \end{center}
% \end{frame}

% \begin{frame}[fragile=singleslide]
%   \frametitle{Mise en œuvre alternative}

%   \begin{minipage}{0.48\linewidth}
% \begin{lstlisting}[basicstyle=\footnotesize\ttfamily\NoAutoSpacing]
% Selectionsort(Data: values[])
%   For i = 0 To <length of values> - 1
%     `{\color{alert}<Find the smallest item}'
%      `{\color{alert}with index j >= i.>}'
%     <Swap values[i] and values[j].>
%   Next i
% End Selectionsort
% \end{lstlisting}
%   \end{minipage}
%   \hfill
%   \begin{minipage}{0.48\linewidth}
% \begin{lstlisting}[basicstyle=\footnotesize\ttfamily\NoAutoSpacing]
% selectionsort <- function(x)
% {
%   xlen <- length(x)
%   for (i in seq_len(xlen))
%   {
%     `{\color{alert}i.min <- (i - 1) +}'
%         `{\color{alert} which.min(x[i:xlen])}'
%     x[c(i, i.min)] <- x[c(i.min, i)]
%   }
%   x
% }
% \end{lstlisting}
%   \end{minipage}
% \end{frame}

% \begin{frame}[plain]
%   \questionbox{Dans le fichier importData.R, à la toute fin de la
%     fonction \code{read.bixi}, il est écrit que «l'utilisation de
%     \code{comment.char = ""} accélère le traitement.» Pourquoi ça accélère
%     le traitement alors qu'un caractère est ajouté? Ça ne devrait pas
%     être le contraire?}
% \end{frame}

% \begin{frame}
%   \frametitle{Ne pas rechercher des commentaires accélère le
%     traitement}

%   \begin{itemize}
%   \item \code{comment.char} est un argument de \code{read.table}
%   \item Valeur par défaut dans \code{read.table}: \code{comment.char =
%       "\#"} \\
%     $\rightarrow$ vérifier la présence de commentaires
%   \item Valeur par défaut dans \code{read.csv}: \code{comment.char = ""} \\
%     $\rightarrow$ ne \alert{pas} vérifier la présence de commentaires
%   \item Extrait de \code{?read.table}: \par\medskip
%     \begin{quote}
%       Using \code{comment.char = ""} will be appreciably faster than the
%       \code{read.table} default.
%     \end{quote}
%   \end{itemize}
% \end{frame}

% \begin{frame}[plain]
%   \questionbox{Comment optimiser un script R pour traiter efficacement
%     des ensembles de données de plusieurs gigaoctets sans épuiser la
%     mémoire vive, en particulier lors de l'utilisation de fonctions
%     d'application de la famille \code{apply} sur de grands tableaux de
%     données?}
% \end{frame}

% \begin{frame}
%   \frametitle{Source de guerres de religion}

%   \begin{itemize}
%   \item Les structures de données natives de R sont toutes gérées dans
%     la mémoire vive de l'ordinateur
%   \item Limite importante à l'époque des systèmes 32~bits \\
%     (mémoire maximale de 4~Go)
%   \item Moins un enjeu aujourd'hui avec les systèmes 64~bits \\
%     (mémoire maximale de \nombre{17 179 869 184}~Go)
%   \item Option populaire pour accélérer le traitement des grands
%     tableaux de données: \pkg{data.table} (introduit un dialecte de R)
%   \end{itemize}
% \end{frame}

% \begin{frame}[plain]
%   \questionbox{Dans les exercices du chapitre 11, à la question 11.1
%     e) et g), nous avons l'expression \code{[.?!]} qui signifie
%     d'obtenir soit \code{.}, \code{?}, ou \code{!}. Serait-il possible
%     d'utiliser à la place \code{|} qui signifie «ou»? Cela fait plus
%     de sens à mes yeux, car je pensais que l'expression voulait plutôt
%     dire qu'on répète le caractère de remplacement zéro ou une fois et
%     qu'on rajoute un \code{!} à la fin plutôt que ce qu'elle veut
%     vraiment dire.}
% \end{frame}

% \begin{frame}
%   \frametitle{Classes de caractères vs alternance}

%   \begin{itemize}
%   \item Dans une classe de caractères \code{[~]}, les opérateurs
%     perdent leur signification \\
%     (sauf \code{\string^} et \code{-})
%   \item Classe de caractères correspond à \alert{un seul} caractère \\
%     \code{cha[iî]ne} \quad\faArrowRight\quad \texttt{chaine} ou \texttt{chaîne}
%   \item Alternance correspond à \alert{tout ce qui précède} ou
%     \alert{tout ce qui suit} l'opérateur \code{|}
%     \code{chai|îne} \quad\faArrowRight\quad \texttt{chai} ou \texttt{îne}
%   \item Classes de caractères permettent les plages de caractères \\
%     \code{[a-z]} plus simple que \code{a|b|c}\dots\code{|y|z}
%   \item Formulation équivalente pour 11.2~g): \\
%     \code{[a-z](\string\.|\string\?|!)( |\string\t)+[A-Z]}
%   \end{itemize}
% \end{frame}

% \begin{frame}[plain]
%   \questionbox{Pourquoi \code{sed} ne remplace-t-il pas le texte
%     directement dans le fichier?}
% \end{frame}

% \begin{frame}[fragile=singleslide]
%   \frametitle{On se fait tous prendre une fois}

%   \code{sed} est un outil de bas niveau.
%   \begin{itemize}
%   \item Lit un fichier une ligne à la fois
%   \item Ne peut pas lire le fichier et le réécrire en même temps
%   \item Conflit possible entre deux opérations pour la même ligne
%   \item Certaines versions comportent un drapeau \code{-i} pour
%     effectuer les changements \\ «\emph{in place}»
%   \item Méthode portable:
%     \begin{Schunk}
% \begin{Verbatim}
% $ sed 's/foo/bar/' foo.txt > foo.tmp && \
%       cp foo.tmp foo.txt && \
%       rm foo.tmp
% \end{Verbatim}
%     \end{Schunk}
%   \end{itemize}
%   Tout ce qui précède s'applique aussi à \code{awk}.
% \end{frame}

% \begin{frame}[plain]
%   \questionbox{Bonjour, pour la première technique de débogage, j'ai
%     de la difficulté à comprendre où utiliser la fonction
%     \code{print}. Vous dites que nous devons la placer à des endroits
%     stratégiques, mais comment pouvons-nous savoir où il est
%     stratégique de les mettre?}
% \end{frame}

% \begin{frame}[fragile]
%   \frametitle{C'est en débogant qu'on devient débogueur}

%   \begin{itemize}[<+->]
%   \item Insérer \code{print} à des endroits où vous voulez connaitre
%     la valeur d'une variable ou identifier le code évalué
%   \item À l'intérieur d'une boucle, pour voir où on en est
%     \begin{Schunk}
% \begin{Verbatim}
% while (...)
% {
%     print(x)
%     ...
% }
% \end{Verbatim}
%     \end{Schunk}
%   \item À l'intérieur d'un \code{if ... else}, pour vérifier quel bloc
%     est évalué
%     \begin{Schunk}
% \begin{Verbatim}
% if (...)
% {
%     print("je suis ici!")
%     ...
% }
% \end{Verbatim}
%     \end{Schunk}
%   \end{itemize}
% \end{frame}


%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "classes-maitre"
%%% coding: utf-8
%%% End:
